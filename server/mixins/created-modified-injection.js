'use strict';

module.exports = function(Model, options) {
  Model.defineProperty('createdBy', {type: Number});
  Model.defineProperty('updatedBy', {type: Number});
  Model.observe('before save', function event(ctx, next) {
    const token = ctx.options && ctx.options.accessToken;
    const userId = token && token.userId;
    if (ctx.instance) {
      if (ctx.isNewInstance) {
        ctx.instance.createdBy = ctx.instance.createdBy || userId;
      } else {
        ctx.instance.updatedBy = ctx.instance.updatedBy || userId;
      }
    } else {
      ctx.data.updatedBy = userId;
    }
    next();
  });
};
