'use strict';

const aws = require('aws-sdk');
const multiparty = require('multiparty');
const {extname} = require('path');
const {readFileSync} = require('fs');

aws.config.update({
  secretAccessKey: process.env.AWS_ACCESS_KEY,
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
});

// Set S3 endpoint to DigitalOcean Spaces
const spacesEndpoint = new aws.Endpoint(process.env.AWS_ENDPOINT);
const s3 = new aws.S3({
  endpoint: spacesEndpoint,
});

/**
 * Helper method which takes the request object and returns a promise with a file.
 */
const getFileFromRequest = (req) => new Promise((resolve, reject) => {
  const form = new multiparty.Form();
  form.parse(req, (err, fields, files) => {
    if (err) reject(err);
    const file = files['file'][0]; // get the file from the returned files object
    if (!file) reject('File was not found in form data.');
    else resolve(file);
  });
});

/**
 * Helper method which takes the request object and returns a promise with the AWS S3 object details.
 */
const uploadFileToS3 = (file, options = {}) => {
  // turn the file into a buffer for uploading
  const buffer = readFileSync(file.path);

  // generate a new random file name
  const fileName = options.name || String(Date.now());

  // the extension of your file
  const extension = extname(file.path);

  // return a promise
  return new Promise((resolve, reject) => {
    return s3.upload({
      Bucket: process.env.AWS_BUCKET,
      ACL: 'public-read',
      Key: `${fileName}${extension}`,
      Body: buffer,
    }, (err, result) => {
      if (err) reject(err);
      else resolve(result); // return the values of the successful AWS S3 request
    });
  });
};

module.exports = (Customer) => {
  Customer.uploadImage = async(id, req, options) => {
    const customer = await Customer.findById(id);
    if (!customer) throw new Error('Customer not found.');

    const file = await getFileFromRequest(req);

    const {Location} = await uploadFileToS3(file);

    customer.photoUrl = Location;

    await customer.save(options);

    return customer;
  };
  Customer.remoteMethod('uploadImage', {
    accepts: [
      {arg: 'id', type: 'string', required: true},
      {arg: 'req', type: 'object', http: {source: 'req'}},
      {arg: 'options', type: 'object', http: 'optionsFromRequest'},
    ],
    returns: {root: true, type: 'object'},
    http: {path: '/:id/photo', verb: 'post'},
  });
};
