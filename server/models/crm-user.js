'use strict';

module.exports = (CRMUser) => {
  CRMUser.beforeRemote('**', (ctx, modelInstance, next) => {
    if (ctx.req.path === '/login') next();
    else {
      const token = ctx.req && ctx.req.accessToken;
      if (!token) {
        let err = new Error('Unauthorized');
        err.statusCode = 401;
        next(err);
      } else {
        const userId = token && token.userId;
        CRMUser.findById(userId, {}, (err, user) => {
          if (err) next(err);
          else if (user.isAdmin) next();
          else {
            let err = new Error('Forbidden');
            err.statusCode = 403;
            next(err);
          }
        });
      }
    }
  });
};
