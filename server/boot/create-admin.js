'use strict';

const server = require('../server');

const User = server.models.CRMUser;

User.create({
  email: process.env.ADMIN_EMAIL,
  password: process.env.ADMIN_PASSWORD,
  isAdmin: true,
}, (err, user) => {
  //
});
