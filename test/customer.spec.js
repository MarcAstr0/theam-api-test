'use strict';

const chai = require('chai');
const request = require('supertest');
const server = require('../server/server');

const expect = chai.expect;

let Customer, User, createdCustomerId, user1Id, user2Id,
  accessToken1, accessToken2;

describe('Customer', () => {
  before('Populating database', (done) => {
    Customer = server.models.Customer;
    User = server.models.CRMUser;
    let clearUsers = User.destroyAll();
    let clearCustomers = Customer.destroyAll();
    Promise.all([clearUsers, clearCustomers]).then(() => {
      let createUser1 = User.create({
        email: 'tester1@theam.io',
        password: '12345',
      });
      let createUser2 = User.create({
        email: 'tester2@theam.io',
        password: '12345',
      });
      Promise.all([createUser1, createUser2])
        .then((users) => {
          user1Id = users[0].id;
          user2Id = users[1].id;
          Customer.create({
            name: 'Homer',
            surname: 'Simpson',
            createdBy: user1Id,
          }, (err, customer) => {
            if (err) done(err);
            createdCustomerId = customer.id;
            let login1 = request(server)
              .post('/api/crm-users/login')
              .send({
                email: 'tester1@theam.io',
                password: '12345',
              });
            let login2 = request(server)
              .post('/api/crm-users/login')
              .send({
                email: 'tester2@theam.io',
                password: '12345',
              });
            Promise.all([login1, login2])
              .then((res) => {
                accessToken1 = res[0].body.id;
                accessToken2 = res[1].body.id;
                done();
              }).catch((err) => {
                done(err);
              });
          });
        })
        .catch((err) => {
          done(err);
        });
    }).catch((err) => {
      done(err);
    });
  });

  after('Cleaning database', (done) => {
    Customer.destroyAll(() => {
      User.destroyAll(() => {
        done();
      });
    });
  });

  describe('GET /customers', () => {
    it('should not allow an unauthenticated user to make a request', (done) => {
      request(server)
        .get('/api/customers')
        .then((res) => {
          expect(res.statusCode).to.equal(401);
          done();
        }).catch(err => {
          done(err);
        });
    });
    it('should return all customers', (done) => {
      request(server)
        .get('/api/customers')
        .set('Authorization', accessToken1)
        .then((res) => {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.be.an('array');
          expect(res.body.length).to.equal(1);
          expect(res.body[0].id).to.equal(createdCustomerId);
          expect(res.body[0].name).to.equal('Homer');
          expect(res.body[0].surname).to.equal('Simpson');
          expect(res.body[0].photoUrl).to.be.null;
          expect(res.body[0].createdBy).to.equal(user1Id);
          expect(res.body[0].updatedBy).to.be.null;
          done();
        }).catch(err => {
          done(err);
        });
    });
  });

  describe('POST /customers', () => {
    it('should not allow an unauthenticated user to make a request', (done) => {
      request(server)
        .post('/api/customers/')
        .send({
          name: 'Peter',
          surname: 'Griffin',
        })
        .then((res) => {
          expect(res.statusCode).to.equal(401);
          done();
        }).catch(err => {
          done(err);
        });
    });
    it('should create a customer', (done) => {
      request(server)
        .post('/api/customers/')
        .set('Authorization', accessToken1)
        .send({
          name: 'Peter',
          surname: 'Griffin',
        })
        .then((res) => {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.be.an('object');
          expect(res.body.id).to.be.a('number');
          expect(res.body.name).to.equal('Peter');
          expect(res.body.surname).to.equal('Griffin');
          expect(res.body.createdBy).to.equal(user1Id);
          expect(res.body.updatedBy).to.be.undefined;
          done();
        }).catch(err => {
          done(err);
        });
    });
  });

  describe('GET /customers/{id}', () => {
    it('should not allow an unauthenticated user to make a request', (done) => {
      request(server)
        .get(`/api/customers/${createdCustomerId}`)
        .then((res) => {
          expect(res.statusCode).to.equal(401);
          done();
        }).catch(err => {
          done(err);
        });
    });
    it('should get a single customer', (done) => {
      request(server)
        .get(`/api/customers/${createdCustomerId}`)
        .set('Authorization', accessToken1)
        .then((res) => {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.be.an('object');
          expect(res.body.id).to.equal(createdCustomerId);
          expect(res.body.name).to.equal('Homer');
          expect(res.body.surname).to.equal('Simpson');
          expect(res.body.photoUrl).to.be.null;
          expect(res.body.createdBy).to.equal(user1Id);
          expect(res.body.updatedBy).to.null;
          done();
        }).catch(err => {
          done(err);
        });
    });
  });

  describe('POST /customer/{id}/photo', () => {
    it('should not allow an unauthenticated user to make a request', (done) => {
      request(server)
        .post(`/api/customers/${createdCustomerId}/photo`)
        .attach('file', './test/homer1.png')
        .then((res) => {
          expect(res.statusCode).to.equal(401);
          done();
        }).catch(err => {
          done(err);
        });
    });
    it('should upload a customer\'s photo', (done) => {
      request(server)
        .post(`/api/customers/${createdCustomerId}/photo`)
        .set('Authorization', accessToken1)
        .attach('file', './test/homer1.png')
        .then((res) => {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.be.an('object');
          expect(res.body.id).to.equal(createdCustomerId);
          expect(res.body.name).to.equal('Homer');
          expect(res.body.surname).to.equal('Simpson');
          expect(res.body.photoUrl).to.not.be.null;
          expect(res.body.createdBy).to.equal(user1Id);
          expect(res.body.updatedBy).to.equal(user1Id);
          done();
        }).catch(err => {
          done(err);
        });
    });
    it('should update a customer\'s existing photo', (done) => {
      Customer.findById(createdCustomerId, {}, (err, customer) => {
        if (err) {
          done(err);
        }
        let oldUrl = customer.photoUrl;
        request(server)
          .post(`/api/customers/${createdCustomerId}/photo`)
          .set('Authorization', accessToken1)
          .attach('file', './test/homer2.jpg')
          .then((res) => {
            expect(res.statusCode).to.equal(200);
            expect(res.body).to.be.an('object');
            expect(res.body.id).to.equal(createdCustomerId);
            expect(res.body.name).to.equal('Homer');
            expect(res.body.surname).to.equal('Simpson');
            expect(res.body.photoUrl).to.not.equal(oldUrl);
            expect(res.body.createdBy).to.equal(user1Id);
            expect(res.body.updatedBy).to.equal(user1Id);
            done();
          }).catch(err => {
            done(err);
          });
      });
    });
  });

  describe('PATCH /customers/{id}', () => {
    it('should not allow an unauthenticated user to make a request', (done) => {
      request(server)
        .patch(`/api/customers/${createdCustomerId}`)
        .send({
          name: 'Al',
          surname: 'Bundy',
        })
        .then((res) => {
          expect(res.statusCode).to.equal(401);
          done();
        }).catch(err => {
          done(err);
        });
    });
    it('should update a customer\'s name and surname', (done) => {
      Customer.findById(createdCustomerId, {}, (err, customer) => {
        if (err) {
          done(err);
        }
        let photoUrl = customer.photoUrl;
        request(server)
          .patch(`/api/customers/${createdCustomerId}`)
          .set('Authorization', accessToken1)
          .send({
            name: 'Al',
            surname: 'Bundy',
          })
          .then((res) => {
            expect(res.statusCode).to.equal(200);
            expect(res.body).to.be.an('object');
            expect(res.body.id).to.equal(createdCustomerId);
            expect(res.body.name).to.equal('Al');
            expect(res.body.surname).to.equal('Bundy');
            expect(res.body.photoUrl).to.equal(photoUrl);
            expect(res.body.createdBy).to.equal(user1Id);
            expect(res.body.updatedBy).to.equal(user1Id);
            done();
          }).catch(err => {
            done(err);
          });
      });
    });
    it('should keep track of the user that updated the customer', (done) => {
      request(server)
        .patch(`/api/customers/${createdCustomerId}`)
        .set('Authorization', accessToken2)
        .send({
          name: 'Homer',
          surname: 'Simpson',
        })
        .then((res) => {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.be.an('object');
          expect(res.body.id).to.equal(createdCustomerId);
          expect(res.body.name).to.equal('Homer');
          expect(res.body.surname).to.equal('Simpson');
          expect(res.body.photoUrl).to.not.be.null;
          expect(res.body.createdBy).to.equal(user1Id);
          expect(res.body.updatedBy).to.equal(user2Id);
          done();
        }).catch(err => {
          done(err);
        });
    });
  });

  describe('DELETE /customers/{id}', () => {
    it('should not allow an unauthenticated user to make a request', (done) => {
      request(server)
        .delete(`/api/customers/${createdCustomerId}`)
        .then((res) => {
          expect(res.statusCode).to.equal(401);
          done();
        }).catch(err => {
          done(err);
        });
    });
    it('should delete a customer', (done) => {
      request(server)
        .delete(`/api/customers/${createdCustomerId}`)
        .set('Authorization', accessToken1)
        .then((res) => {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.be.an('object');
          expect(res.body.count).to.equal(1);
          done();
        }).catch(err => {
          done(err);
        });
    });
  });
});
