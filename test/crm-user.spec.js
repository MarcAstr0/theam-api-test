'use strict';

const chai = require('chai');
const request = require('supertest');
const server = require('../server/server');

const expect = chai.expect;

let User, adminUserId, regularUser1Id, regularUser2Id, regularUser3Id,
  adminAccessToken, regularAccessToken;

describe('CRM User', () => {
  before('Populating database', (done) => {
    User = server.models.CRMUser;
    let createAdminUser = User.create({
      email: 'admin@theam.io',
      password: '12345',
      isAdmin: true,
    });
    let createRegularUser1 = User.create({
      email: 'tester1@theam.io',
      password: '12345',
    });
    let createRegularUser2 = User.create({
      email: 'tester2@theam.io',
      password: '12345',
    });
    Promise.all([createAdminUser, createRegularUser1, createRegularUser2])
      .then((users) => {
        adminUserId = users[0].id;
        regularUser1Id = users[1].id;
        regularUser2Id = users[2].id;
        let loginAdmin = request(server)
          .post('/api/crm-users/login')
          .send({
            email: 'admin@theam.io',
            password: '12345',
          });
        let loginRegularUser = request(server)
          .post('/api/crm-users/login')
          .send({
            email: 'tester1@theam.io',
            password: '12345',
          });
        Promise.all([loginAdmin, loginRegularUser])
          .then((responses) => {
            adminAccessToken = responses[0].body.id;
            regularAccessToken = responses[1].body.id;
            done();
          })
          .catch(err => {
            done(err);
          });
      })
      .catch(err => {
        done(err);
      });
  });

  after('Cleaning database', (done) => {
    User.destroyAll(done);
  });

  describe('Admin user', () => {
    it('should be created when the app starts', (done) => {
      User.find({where: {email: process.env.ADMIN_EMAIL}}, (err, users) => {
        if (err) done(err);
        expect(users.length).to.equal(1);
        expect(users[0].email).to.equal(process.env.ADMIN_EMAIL);
        expect(users[0].isAdmin).to.be.true;
        done();
      });
    });
  });

  describe('POST /crm-users', () => {
    it('should not allow an unauthenticated user to make a request', (done) => {
      request(server)
        .post('/api/crm-users')
        .send({
          email: 'tester3@theam.io',
          password: '12345',
        })
        .then((res) => {
          expect(res.statusCode).to.equal(401);
          done();
        })
        .catch(err => {
          done(err);
        });
    });
    it('should forbid a non admin user to create a user', (done) => {
      request(server)
        .post('/api/crm-users')
        .set('Authorization', regularAccessToken)
        .send({
          email: 'tester3@theam.io',
          password: '12345',
        })
        .then((res) => {
          expect(res.statusCode).to.equal(403);
          done();
        })
        .catch(err => {
          done(err);
        });
    });
    it('should allow an admin user to create a user', (done) => {
      request(server)
        .post('/api/crm-users')
        .set('Authorization', adminAccessToken)
        .send({
          email: 'tester3@theam.io',
          password: '12345',
        })
        .then((res) => {
          expect(res.statusCode).to.equal(200);
          expect(res.body.id).to.be.a('number');
          expect(res.body.email).to.equal('tester3@theam.io');
          regularUser3Id = res.body.id;
          done();
        })
        .catch(err => {
          done(err);
        });
    });
  });

  describe('DELETE /crm-users/{id}', () => {
    it('should not allow an unauthenticated user to make a request', (done) => {
      request(server)
        .delete(`/api/crm-users/${regularUser2Id}`)
        .then((res) => {
          expect(res.statusCode).to.equal(401);
          done();
        })
        .catch(err => {
          done(err);
        });
    });
    it('should forbid a non admin user to delete a user', (done) => {
      request(server)
        .delete(`/api/crm-users/${regularUser2Id}`)
        .set('Authorization', regularAccessToken)
        .then((res) => {
          expect(res.statusCode).to.equal(403);
          done();
        })
        .catch(err => {
          done(err);
        });
    });
    it('should allow an admin user to delete a user', (done) => {
      request(server)
        .delete(`/api/crm-users/${regularUser2Id}`)
        .set('Authorization', adminAccessToken)
        .then((res) => {
          expect(res.statusCode).to.equal(200);
          expect(res.body.count).to.equal(1);
          done();
        })
        .catch(err => {
          done(err);
        });
    });
  });

  describe('PATCH /crm-users/{id}', () => {
    it('should not allow an unauthenticated user to make a request', (done) => {
      request(server)
        .patch(`/api/crm-users/${regularUser3Id}`)
        .send({
          email: 'new-email@theam.io',
        })
        .then((res) => {
          expect(res.statusCode).to.equal(401);
          done();
        })
        .catch(err => {
          done(err);
        });
    });
    it('should forbid a non admin user to update a user', (done) => {
      request(server)
        .patch(`/api/crm-users/${regularUser3Id}`)
        .set('Authorization', regularAccessToken)
        .send({
          email: 'new-email@theam.io',
        })
        .then((res) => {
          expect(res.statusCode).to.equal(403);
          done();
        })
        .catch(err => {
          done(err);
        });
    });
    it('should allow an admin user to update a user', (done) => {
      request(server)
        .patch(`/api/crm-users/${regularUser3Id}`)
        .set('Authorization', adminAccessToken)
        .send({
          email: 'new-email@theam.io',
        })
        .then((res) => {
          expect(res.statusCode).to.equal(200);
          expect(res.body.id).to.equal(regularUser3Id);
          expect(res.body.email).to.equal('new-email@theam.io');
          done();
        })
        .catch(err => {
          done(err);
        });
    });
    it('should be able to change a user\'s admin status', (done) => {
      request(server)
        .patch(`/api/crm-users/${regularUser3Id}`)
        .set('Authorization', adminAccessToken)
        .send({
          isAdmin: true,
        })
        .then((res) => {
          expect(res.statusCode).to.equal(200);
          expect(res.body.id).to.equal(regularUser3Id);
          expect(res.body.isAdmin).to.be.true;
          done();
        })
        .catch(err => {
          done(err);
        });
    });
  });

  describe('GET /crm-users', () => {
    it('should not allow an unauthenticated user to make a request', (done) => {
      request(server)
        .get('/api/crm-users')
        .then((res) => {
          expect(res.statusCode).to.equal(401);
          done();
        })
        .catch(err => {
          done(err);
        });
    });
    it('should forbid a non admin user to list users', (done) => {
      request(server)
        .get('/api/crm-users')
        .set('Authorization', regularAccessToken)
        .then((res) => {
          expect(res.statusCode).to.equal(403);
          done();
        })
        .catch(err => {
          done(err);
        });
    });
    it('should allow an admin user to list users', (done) => {
      request(server)
        .get('/api/crm-users')
        .set('Authorization', adminAccessToken)
        .then((res) => {
          expect(res.statusCode).to.equal(200);
          expect(res.body).to.be.an('array');
          expect(res.body.length).to.equal(4);
          done();
        })
        .catch(err => {
          done(err);
        });
    });
  });
});
