module.exports = {
  apps: [{
    name: 'API',
    script: 'server/server.js',
    instances: 'max',
    exec_mode: 'cluster',
  }],
};
