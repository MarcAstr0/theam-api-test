# API Test - The CRM service

This project is part of the second phase of [The Agile Monkeys'](https://www.theagilemonkeys.com/) recruitment process.
The goal of the project is to build a REST API to manage customer data for a small shop.

## Table of contents
- [Development and testing environment setup](#development-and-testing-environment-setup)
  * [Requirements](#requirements)
  * [Dependency installation](#dependency-installation)
  * [Environment variable setup](#environment-variable-setup)
  * [Database table creation](#database-table-creation)
  * [Running tests](#running-tests)
  * [Running the API server](#running-the-api-server)
- [API documentation](#api-documentation)
  * [Authenticating requests](#authenticating-requests)
  * [Endpoints](#endpoints)
    + [/crm-users](#crm-users)
    + [/customers](#customers)

## Development and testing environment setup

### Requirements

For development and running the API locally one only needs to have a [Node.js](https://nodejs.org/en/) distribution
installed and access to a database. This distribution in particular was built with a connection to a MySQL
database in mind, but in reality one can use any of the [RDBMS connectors supported by the
LoopBack framework](https://loopback.io/doc/en/lb3/Database-connectors.html) and setting it
accordingly in the `datasources.<environment>.json` files.

### Dependency installation

First one must clone the project from the repository:

```
$ git clone git@gitlab.com:MarcAstr0/theam-api-test.git
```

Then install dependencies using npm:

```
$ cd theam-api-test
$ npm install
```

### Environment variable setup

In order for the API to work a number of environment variables need to be set. Instead
of setting these variables at a system level, it is strongly recommend using the [dotenv](https://www.npmjs.com/package/dotenv)
package (it is installed as a dependency in the previous step). This package expects
a `.env` file at the root of the project and the contents of this file must be something
like the following:

```bash
# Production database settings
DB_HOST=
DB_PORT=
DB_NAME=
DB_USER=
DB_PASSWORD=

# Test database settings
TEST_DB_HOST=
TEST_DB_PORT=
TEST_DB_NAME=
TEST_DB_USER=
TEST_DB_PASSWORD=

# Development database settings
DEV_DB_HOST=
DEV_DB_PORT=
DEV_DB_NAME=
DEV_DB_USER=
DEV_DB_PASSWORD=

# S3 settings
AWS_ENDPOINT=
AWS_BUCKET=
AWS_ACCESS_KEY_ID=
AWS_ACCESS_KEY=

# API root user settings
ADMIN_EMAIL=
ADMIN_PASSWORD=
```

The following table describes what values must go for each variable:

| Variable | Description |
|---|---|
| `DB_HOST` | Hostname or IP address of the production database server |
| `DB_PORT` | Production database port number |
| `DB_NAME` | Production database name |
| `DB_USER` | Production database user |
| `DB_PASSWORD` | Password for the production database user|
| `TEST_DB_HOST` | Hostname or IP address of the test database server |
| `TEST_DB_PORT` | Test database port number |
| `TEST_DB_NAME` | Test database name |
| `TEST_DB_USER` | Test database user |
| `TEST_DB_PASSWORD` | Test for the production database user|
| `DEV_DB_HOST` | Hostname or IP address of the development database server |
| `DEV_DB_PORT` | Development database port number |
| `DEV_DB_NAME` | Development database name |
| `DEV_DB_USER` | Development database user |
| `DEV_DB_PASSWORD` | Password for the development database user|
| `AWS_ENDPOINT` | Endpoint of an [Amazon S3](https://aws.amazon.com/s3/?nc1=h_ls) compatible storage |
| `AWS_BUCKET` | S3 compatible storage bucket name |
| `AWS_ACCESS_KEY_ID` | Access key ID for the S3 compatible storage |
| `AWS_ACCESS_KEY` | Access key for the S3 compatible storage |
| `ADMIN_EMAIL` | E-mail address for the API's root user |
| `ADMIN_PASSWORD` | Password for the API's root user |

### Database table creation

A number of tables must be created in the production, development and test
databases. The following SQL script uses the MySQL keyword `auto_increment`
for the ID columns. If one were to use a different RDBMS one must change
these columns with the corresponding default value (like using `SERIAL` in
a PostgreSQL database).

```sql
create table if not exists ACL
(
	id int auto_increment
		primary key,
	model varchar(512) null,
	property varchar(512) null,
	accessType varchar(512) null,
	permission varchar(512) null,
	principalType varchar(512) null,
	principalId varchar(512) null
);

create table if not exists AccessToken
(
	id varchar(255) not null
		primary key,
	ttl int null,
	scopes text null,
	created datetime null,
	userId int null
);

create table if not exists CRMUser
(
	id int auto_increment
		primary key,
	password varchar(512) not null,
	email varchar(512) not null,
	isAdmin tinyint(1) default 0 null
);

create table if not exists Customer
(
	id bigint auto_increment
		primary key,
	name varchar(255) not null,
	surname varchar(255) not null,
	photoUrl varchar(255) null,
	createdBy bigint null,
	updatedBy bigint null
);

create table if not exists Role
(
	id int auto_increment
		primary key,
	name varchar(512) not null,
	description varchar(512) null,
	created datetime null,
	modified datetime null
);

create table if not exists RoleMapping
(
	id int auto_increment
		primary key,
	principalType varchar(512) null,
	principalId varchar(255) null,
	roleId int null
);

create index principalId
	on RoleMapping (principalId);
```

### Running tests

Once all environment variables have been set and the tables have been created
on the database the following command will perform automated tests:

```
$ npm test
```

**WARNING**: make sure you are using different databases for the production
and test environments, since running tests will *clear all data* on the test
database tables.

### Running the API server

To run the API server simply do:

```
$ node .
```

and the API will be running on localhost on port 3000.

## API documentation

Base URL:

```
https://theam-api-test.mariocastro.dev/
```

### Authenticating requests

All API requests must be authenticated. Authentication is performed via
access tokens sent in Authorization header of the request. In order to 
get an access token one must login using the `/crm-users/login` endpoint:

<dl>
  <dt>Description</dt>
  <dd>This endpoint is used to authenticate a user using his e-mail address and password.</dd>
  <dt>URL structure</dt>
  <dd><pre>https://theam-api-test.mariocastro.dev/api/crm-users/login</pre></dd>
  <dt>Method</dt>
  <dd>POST</dd>
  <dt>Example</dt>
  <dd>
  <pre>curl -X POST \
  -d email=&lt;USER_EMAIL&gt; \
  -d password=&lt;USER_PASSWORD&gt; \
  https://theam-api-test.mariocastro.dev/api/crm-users/login</pre>
  </dd>
  <dt>Parameters</dt>
  <dd>
    <table>
    <tr>
    <td><code>email</code></td>
    <td><i>String</i></td>
    <td>The user's e-mail address</td>
    </tr>
    <tr>
    <td><code>password</code></td>
    <td><i>String</i></td>
    <td>The user's password</td>
    </tr>
    </table>
  </dd>
  <dt>Returns</dt>
  <dd>A JSON-encoded dictionary with the access token (<code>id</code>), time-to-live in seconds (<code>ttl</code>), timestamp when the access token was created (<code>created</code>) and the user's ID (<code>userId</code>).<br>
  Sample response:<br>
  <pre>{  
  "id":"tnjnoq69ZBpdFCBEVguhehM2FIdtGQ6eqwUyRWzi3URqAV27TBSjUgVewaUIobmi",
  "ttl":1209600,
  "created":"2019-09-20T03:09:26.062Z",
  "userId":1
}</pre>
  </dd>
</dl>

### Endpoints

#### /crm-users

##### `POST /crm-users`

<dl>
  <dt>Description</dt>
  <dd>This endpoint is used to create a new CRM user. Only users with administration privileges are allowed to create new users.</dd>
  <dt>URL structure</dt>
  <dd><pre>https://theam-api-test.mariocastro.dev/api/crm-users</pre></dd>
  <dt>Method</dt>
  <dd>POST</dd>
  <dt>Example</dt>
  <dd>
  <pre>curl -X POST \
  -H "Authorization: &lt;ACCESS_TOKEN&gt;" \
  -d email=&lt;USER_EMAIL&gt; \
  -d password=&lt;USER_PASSWORD&gt; \
  https://theam-api-test.mariocastro.dev/api/crm-users</pre>
  </dd>
  <dt>Parameters</dt>
  <dd>
    <table>
    <tr>
    <td><code>email</code></td>
    <td><i>String</i></td>
    <td>The new user's e-mail address</td>
    </tr>
    <tr>
    <td><code>password</code></td>
    <td><i>String</i></td>
    <td>The new user's password</td>
    </tr>
    </table>
  </dd>
  <dt>Returns</dt>
  <dd>A JSON-encoded dictionary with the new user's e-mail address (<code>email</code>) and ID (<code>id</code>).<br>
  Sample response:<br>
  <pre>{  
  "email":"tester@theam.io",
  "id":2
}</pre>
  </dd>
</dl>

##### `GET /crm-users`

<dl>
  <dt>Description</dt>
  <dd>This endpoint is used to list all CRM users. Only users with administration privileges are allowed to list users.</dd>
  <dt>URL structure</dt>
  <dd><pre>https://theam-api-test.mariocastro.dev/api/crm-users</pre></dd>
  <dt>Method</dt>
  <dd>GET</dd>
  <dt>Example</dt>
  <dd>
  <pre>curl -H "Authorization: &lt;ACCESS_TOKEN&gt;" \
  https://theam-api-test.mariocastro.dev/api/crm-users</pre>
  </dd>
  <dt>Parameters</dt>
  <dd>None</dd>
  <dt>Returns</dt>
  <dd>An array of JSON-encoded dictionaries with the user's e-mail address (<code>email</code>), ID (<code>id</code>) and admin status (<code>isAdmin</code>).<br>
  Sample response:<br>
  <pre>[  
  {  
    "isAdmin":true,
    "email":"root@theam.io",
    "id":1
  },
  {  
    "isAdmin":false,
    "email":"tester@theam.io",
    "id":2
  }
]</pre>
  </dd>
</dl>

##### `PATCH /crm-users/{id}`

<dl>
  <dt>Description</dt>
  <dd>This endpoint is used to modify an existing CRM user identified by its ID. Only users with administration privileges are allowed to modify users.</dd>
  <dt>URL structure</dt>
  <dd><pre>https://theam-api-test.mariocastro.dev/api/crm-users/{id}</pre></dd>
  <dt>Method</dt>
  <dd>PATCH</dd>
  <dt>Example</dt>
  <dd>
  <pre>curl -X PATCH \
  -H "Authorization: &lt;ACCESS_TOKEN&gt;" \
  -d email=&lt;NEW_EMAIL&gt; \
  https://theam-api-test.mariocastro.dev/api/crm-users/2</pre>
  </dd>
  <dt>Parameters</dt>
  <dd>
    <table>
    <tr>
    <td><code>email</code></td>
    <td><i>String?</i></td>
    <td>The user's new e-mail address</td>
    </tr>
    <tr>
    <td><code>password</code></td>
    <td><i>String?</i></td>
    <td>The user's new password</td>
    </tr>
    <tr>
    <td><code>isAdmin</code></td>
    <td><i>Boolean?</i></td>
    <td>The user's new admin status</td>
    </tr>
    </table>
  </dd>
  <dt>Returns</dt>
  <dd>A JSON-encoded dictionary with the modified user's e-mail address (<code>email</code>), ID (<code>id</code>) and admin status (<code>isAdmin</code>).<br>
  Sample response:<br>
  <pre>{  
  "isAdmin":false,
  "email":"new_email@theam.io",
  "id":2
}</pre>
  </dd>
</dl>

##### `DELETE /crm-users/{id}`

<dl>
  <dt>Description</dt>
  <dd>This endpoint is used delete a single CRM user identified by its ID. Only users with administration privileges are allowed to delete users.</dd>
  <dt>URL structure</dt>
  <dd><pre>https://theam-api-test.mariocastro.dev/api/crm-users/{id}</pre></dd>
  <dt>Method</dt>
  <dd>DELETE</dd>
  <dt>Example</dt>
  <dd>
  <pre>curl -X DELETE \
  -H "Authorization: &lt;ACCESS_TOKEN&gt;" \
  https://theam-api-test.mariocastro.dev/api/crm-users/2</pre>
  </dd>
  <dt>Parameters</dt>
  <dd>None</dd>
  <dt>Returns</dt>
  <dd>A JSON-encoded dictionary with the number of affected resources (<code>count</code>).<br>
  Sample response:<br>
  <pre>{  
   "count":1
}</pre>
  </dd>
</dl>

#### /customers

##### `POST /customers`

<dl>
  <dt>Description</dt>
  <dd>This endpoint is used to create a new customer.</dd>
  <dt>URL structure</dt>
  <dd><pre>https://theam-api-test.mariocastro.dev/api/customers</pre></dd>
  <dt>Method</dt>
  <dd>POST</dd>
  <dt>Example</dt>
  <dd>
  <pre>curl -X POST \
  -H "Authorization: &lt;ACCESS_TOKEN&gt;" \
  -d name=&lt;CUSTOMER_NAME&gt; \
  -d surname=&lt;CUSTOMER_SURNAME&gt; \
  https://theam-api-test.mariocastro.dev/api/customers</pre>
  </dd>
  <dt>Parameters</dt>
  <dd>
    <table>
    <tr>
    <td><code>name</code></td>
    <td><i>String</i></td>
    <td>The new customers's name</td>
    </tr>
    <tr>
    <td><code>surname</code></td>
    <td><i>String</i></td>
    <td>The new customer's surname</td>
    </tr>
    </table>
  </dd>
  <dt>Returns</dt>
  <dd>A JSON-encoded dictionary with the new customer's ID (<code>id</code>), name (<code>name</code>), surname (<code>surname</code>) and the ID of the user that created the customer (<code>createdBy</code>).<br>
  Sample response:<br>
  <pre>{  
  "id":2,
  "name":"Peter",
  "surname":"Griffin",
  "createdBy":1
}</pre>
  </dd>
</dl>

##### `GET /customers`

<dl>
  <dt>Description</dt>
  <dd>This endpoint is used to list all customers.</dd>
  <dt>URL structure</dt>
  <dd><pre>https://theam-api-test.mariocastro.dev/api/customers</pre></dd>
  <dt>Method</dt>
  <dd>GET</dd>
  <dt>Example</dt>
  <dd>
  <pre>curl -H "Authorization: &lt;ACCESS_TOKEN&gt;" \
  https://theam-api-test.mariocastro.dev/api/customers</pre>
  </dd>
  <dt>Parameters</dt>
  <dd>None</dd>
  <dt>Returns</dt>
  <dd>An array of JSON-encoded dictionaries with the user's ID (<code>id</code>), name (<code>name</code>), surname (<code>surname</code>), URL with the user's photo (<code>photoUrl</code>), ID of the user that created the customer (<code>createdBy</code>) and ID of the user that modified the customer (<code>updatedBy</code>).<br>
  Sample response:<br>
  <pre>[  
  {  
     "id":1,
     "name":"Homer",
     "surname":"Simpson",
     "photoUrl":"https://mariocastro-dev.nyc3.digitaloceanspaces.com/1568998357118.jpg",
     "createdBy":1,
     "updatedBy":3
  },
  {  
     "id":2,
     "name":"Peter",
     "surname":"Griffin",
     "photoUrl":null,
     "createdBy":1,
     "updatedBy":null
  }
]</pre>
  </dd>
</dl>

##### `GET /customers/{id}`

<dl>
  <dt>Description</dt>
  <dd>This endpoint is used to get a single customer identified by its ID.</dd>
  <dt>URL structure</dt>
  <dd><pre>https://theam-api-test.mariocastro.dev/api/customers/{id}</pre></dd>
  <dt>Method</dt>
  <dd>GET</dd>
  <dt>Example</dt>
  <dd>
  <pre>curl -H "Authorization: &lt;ACCESS_TOKEN&gt;" \
  https://theam-api-test.mariocastro.dev/api/customers/1</pre>
  </dd>
  <dt>Parameters</dt>
  <dd>None</dd>
  <dt>Returns</dt>
  <dd>A JSON-encoded dictionary with the user's ID (<code>id</code>), name (<code>name</code>), surname (<code>surname</code>), URL with the user's photo (<code>photoUrl</code>), ID of the user that created the customer (<code>createdBy</code>) and ID of the user that modified the customer (<code>updatedBy</code>).<br>
  Sample response:<br>
  <pre>{  
  "id":1,
  "name":"Homer",
  "surname":"Simpson",
  "photoUrl":"https://mariocastro-dev.nyc3.digitaloceanspaces.com/1568998357118.jpg",
  "createdBy":1,
  "updatedBy":3
}</pre>
  </dd>
</dl>

##### `PATCH /customers/{id}`

<dl>
  <dt>Description</dt>
  <dd>This endpoint is used to modify an existing customer's name or surname.</dd>
  <dt>URL structure</dt>
  <dd><pre>https://theam-api-test.mariocastro.dev/api/customers/{id}</pre></dd>
  <dt>Method</dt>
  <dd>PATCH</dd>
  <dt>Example</dt>
  <dd>
  <pre>curl -X PATCH \
  -H "Authorization: &lt;ACCESS_TOKEN&gt;" \
  -d name=&lt;NEW_NAME&gt; \
  -d surname=&lt;NEW_SURNAME&gt; \
  https://theam-api-test.mariocastro.dev/api/customers/2</pre>
  </dd>
  <dt>Parameters</dt>
  <dd>
    <table>
    <tr>
    <td><code>name</code></td>
    <td><i>String?</i></td>
    <td>The customers's new name</td>
    </tr>
    <tr>
    <td><code>surname</code></td>
    <td><i>String?</i></td>
    <td>The customer's new surname</td>
    </tr>
    </table>
  </dd>
  <dt>Returns</dt>
  <dd>A JSON-encoded dictionary with the user's ID (<code>id</code>), name (<code>name</code>), surname (<code>surname</code>), URL with the user's photo (<code>photoUrl</code>), ID of the user that created the customer (<code>createdBy</code>) and ID of the user that modified the customer (<code>updatedBy</code>).<br>
  Sample response:<br>
  <pre>{  
  "id":2,
  "name":"Al",
  "surname":"Bundy",
  "photoUrl":null,
  "createdBy":1,
  "updatedBy":1
}</pre>
  </dd>
</dl>

##### `DELETE /customers/{id}`

<dl>
  <dt>Description</dt>
  <dd>This endpoint is used to delete a single customer identified by its ID.</dd>
  <dt>URL structure</dt>
  <dd><pre>https://theam-api-test.mariocastro.dev/api/customers/{id}</pre></dd>
  <dt>Method</dt>
  <dd>DELETE</dd>
  <dt>Example</dt>
  <dd>
  <pre>curl -X DELETE \
  -H "Authorization: &lt;ACCESS_TOKEN&gt;" \
  https://theam-api-test.mariocastro.dev/api/customers/2</pre>
  </dd>
  <dt>Parameters</dt>
  <dd>None</dd>
  <dt>Returns</dt>
  <dd>A JSON-encoded dictionary with the number of affected resources (<code>count</code>).<br>
  Sample response:<br>
  <pre>{  
 "count":1
}</pre>
  </dd>
</dl>

##### `POST /customers/{id}/photo`

<dl>
  <dt>Description</dt>
  <dd>This endpoint is used to add a photo to an existing customer, identified by its ID.</dd>
  <dt>URL structure</dt>
  <dd><pre>https://theam-api-test.mariocastro.dev/api/customers/{id}/photo</pre></dd>
  <dt>Method</dt>
  <dd>POST</dd>
  <dt>Example</dt>
  <dd>
  <pre>curl -H "Authorization: &lt;ACESS_TOKEN&gt;" \
  -F file=@&lt;PATH_TO_FILE&gt; \
  https://theam-api-test.mariocastro.dev/api/customers/1/photo</pre>
  </dd>
  <dt>Parameters</dt>
  <dd>
    <table>
    <tr>
    <td><code>file</code></td>
    <td><i>File</i></td>
    <td>The customer's photo file</td>
    </tr>
    </table>
  </dd>
  <dt>Returns</dt>
  <dd>A JSON-encoded dictionary with the user's ID (<code>id</code>), name (<code>name</code>), surname (<code>surname</code>), URL with the user's photo (<code>photoUrl</code>), ID of the user that created the customer (<code>createdBy</code>) and ID of the user that modified the customer (<code>updatedBy</code>).<br>
  Sample response:<br>
  <pre>{  
  "id":1,
  "name":"Homer",
  "surname":"Simpson",
  "photoUrl":"https://mariocastro-dev.nyc3.digitaloceanspaces.com/1569000028980.jpg",
  "createdBy":1,
  "updatedBy":3
}</pre>
  </dd>
</dl>
